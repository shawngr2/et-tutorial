**Einstein Toolkit Tutorial**



## Part 1: Compiling

# 1) Download

Use the GetComponents script to download the thorns and make needed directories by running the following commands
```
curl -kLO https://raw.githubusercontent.com/gridaphobe/CRL/master/GetComponents;
chmod a+x GetComponents;
./GetComponents --parallel https://bitbucket.org/shawngr2/et-tutorial/raw/master/mythornlist3.th;
mkdir /scratch/sciteam/$USER/simulations;
```

# 2) Fix Broken Links

These are due to the an issue with the ExternalLibrary thorns.  To fix this, run
```
cd Cactus/arrangements/ExternalLibraries;
for file in *; do rm $file; ln -s ../../repos/ExternalLibraries-$file $file; done;
cd ../..;
ln -s /scratch/sciteam/$USER/simulations simulations;
```


# 3) Set Setup Simfactory for Bluewaters
This lets the system know which libraries and modules to use (note you should currently be in the Cactus directory). Perform the setup with the command below.
```
./simfactory/bin/sim setup --optionlist=simfactory/mdb/optionlists/bluewaters-gnu.cfg
``` 
It will ask you to set name value pairs.  The pairs compose the file `simfactory/etc/defs.local.ini` (assuming you are in the Cactus directory). If you mess up, I reccommend just editing the file directly.  Here is what your file should look like, replacing the values in angle brackets with your info
```
[default]
user            = <your_username>
email           = <your_email>
sourcebasedir   = /u/sciteam/@USER@
basedir         = /scratch/sciteam/@USER@/simulations/

[bluewaters]
allocation      = baws
```

# 4) Compile the ET 

Use the following command.  Note this takes ~30 minutes
```
./simfactory/bin/sim build ET --thornlist=thornlists/mythornlist3.th
```
This creates an executable exe/cactus_ET and the configuration configs/ET.  If we omit "ET" from the line above, the default configuration name is sim.

Note that this compiles using the thornlist from this repo.  If you modify a thorn, can can recompile using `./simfactory/bin/sim build ET`
without specifying the thornlist.

# 5) Test

Test the compilation to make sure everything looks ok by running an isolated neutron star known as a TOV star.  To run this simulation, enter the following command
```
./simfactory/bin/sim submit TOV --parfile=par/static_tov.par --procs=32 --walltime=0:30:0 --configuration=ET --queue=debug  --allocation=baws;
```
The results of the simulation are in the simulations/TOV directory.  Since this is the first simulation named TOV that you have run, that results are stored in the simulations/TOV/output-0000 subdirectory, while the results of subsequent runs will be stored in output-0001, output-0002, etc. 

To check if the simulation is running, you should go into the results directory using 
```
cd simulations/TOV/output-0000;
```
If the simulation is running, the file showing the output of the simulation will be called something like `9983536.bw.OU` where the number 9983536 here is the job id.  If the simulation is finished, the name of the simulation output file will be changed to `TOV.out`.

---

## Part 2: Using

# 1) Run a BBH Simulation

Overnight, run a BBH simulation.  To run the simulation, use the following command
```
./simfactory/bin/sim submit BBH --parfile=par/qc0-mclachlan.par --procs=64 --walltime=12:0:0 --configuration=ET --queue=normal  --allocation=baws;
```

# 2) Black Hole Trajectories

For this section of the tutorial you will need an x11 server.  I personally recommend Mobaxterm https://mobaxterm.mobatek.net/ for those of you on Windows.  Download and install Mobaxterm if needed and then add a session for bluewaters and enter the same info you would use to ssh into BW.

Now let us look at the simulations results for the black hole trajectories.  First go into the directory where the simulation resuls are located.  The location of the outputs is determined by the parameter file where we set `IO::out_dir = $parfile`. So let us move to that directory with the command
```
cd Cactus/simulations/BBH/output-0000/qc0-mclachlan/;
```
Now, let me introduce you to one of the simplest, user-friendly plotting tools called gnuplot http://www.gnuplot.info/documentation.html. The BH trajectories are given in the files `BH_diagnostics.ah1.gp`, `BH_diagnostics.ah2.gp`, for `BH_diagnostics.ah3.gp` for the primary, secondary, and mergered BHs respectively (I honestly am not sure what the others are for).  If we peak at those files we note that the `centroid_x` and `centroid_y` variables are in columns 3 and 4 respectively, so lets plot them all with gnuplot using the following commands
```
gnuplot;
```
The once gnuplot opens, run these commands
```
p 'BH_diagnostics.ah1.gp' u 3:4 w lp;
rep 'BH_diagnostics.ah2.gp' u 3:4 w lp;
rep 'BH_diagnostics.ah3.gp' u 3:4 w lp;
```
As for the commands used, `p` is a shortand for `plot`, then we put the filename, `u` is a shorthand for `using` which will specify what columns we want to plot, `w` is a shorhand for `with` that specifies format options, and `lp` is a shorhand for `linepoints` the format we want to use.  Finally, `rep` is a shorthand for `replot`, which tells gnuplot to plot a new plot without clearing the previous plots.

Looking at the plot of the trajectories, we notice that, at most, only a single orbit occurred prior to merger.  The central black hole does not move, which makes sense as both BHs were equal mass prior to merger. 


### Plot of BBH Trajectories
![BBH_Trajectories](plots/bbh_trajectories.png)


# 3) Gravitational Waves

GW are not extracted directly from the simulation.  Instead, we extract a quantity called the psi4 Weyl scalar (projections of the Riemann tensor that can be calculated using the local variables calculated during the simulation).  psi4 is related to the graviational waves via psi4 = d^2(h_+ - ih_x)/dt^2.  The conversion from psi4 to GW reduces the strength of high frequency signals.

We extract psi4 from simulations by calculating Weyl psi4 scalar at every point and then projecting it at spheres of various radii.  Remember, when we detect GW, we only see it a one particular distance away, hence we mimic this via the afformentioned projection on to spheres.  This projection breaks the GW into its spherical harmonic components which are specified by l and m.  The harmonics are the l=2 m=-2,+2 mode, though several others are contribute especially when dealling with eccentric signals.  However, for the most part, the GW strength is synomous with the strength of the l=2 m=2 mode.  The l=2 m=2 mode has a frequency twice that of the orbital frequency.


Now, use gnuplot to look at various GW.  The files are called `mp_Psi4_l<l mode number>_m<m mode number>_r<detecctor distance>.asc` where the angle backets are replaced by numbers repressenting the quantity described in the angle bracket.  The first column is time, the second column is the real part of psi4, and the 3rd column is the imaginary part of psi4.  For example, we can plot the l=2, m=2, mode of the detector at a distance of 40M via
```
p 'mp_Psi4_l2_m2_r40.00.asc' u 1:2 w l;
```

### Plot of Psi4 at 40M
![psi4](plots/psi4_r40.png)

Now exit gnuplot with the following command
```
q;
```

# 4) 3D Visualizations

We will use VisIt to look at the results of the simulations in more detail.  First load the VisIt module using
```
module load visit;
visit &;
```
Now open the HDF5 files for the lapse.  Lapse represents the passage of coordinate time a each grid point.  The lower the lapse, the slower time moves due to gravitational time dilation (under these gauge conditions, but there are still coordinate effects).  The lapse will be used as to identify the location of the black holes.

1. Open the files by hitting `ctrl+o`
1. Set "Open file as type" to `CarpetHDF5`
1. Double click on the group of files labeled `admbase-lapse.file_* database`
1. Add a contour plot by click the menu `Add->Contour->ADMBASE--alp`
1. Double click on contour in the plots menu
1. Set a single contour with a value of 0.3 in your favorite color (this represents the black hole)
1. Add a relection opperator via `Operators->Transformations->Reflect`
1. Double click on the Reflect
1. Set relections to be 3D, Specify XYZ to 0, and select the reflection ocants to be the original +x+y+z, +x+y-z, -x-y+z, and -x-y-z.
1. Click on draw and hit play to move the data forwards in time.
1. Finally experiment by adding a mesh via `Add->Mesh->Carpet AMR-grid`

# 5) A Closer Look at Parameter Files

Go into the `par/` directory via the following commands
```
cd;
cd Cactus/par;
```
Open the file `qc0-mclachlan.par` with your favorite text editor.  This file is also included at the end of this tutorial.

The file specifies what thorns we are going to use and set variables to tell us how we are going to use them.  Thorns can be more or less divided into several catagories.

1. Simulation Managment
1. Grid Setup
1. Variable Storage
1. Initial Data
1. Evolution
1. Analysis
1. I/O

When first looking at all these thorns and the parameters in the .par file, it can seem overwhelming.  To get more information about the thorns, we can look at the .ccl files.  We will now look at the Multipole thorn as an example.  This is an analysis thorn that projects grid functions onto spherical shells, mostly used for GW extraction.  Move to the directory where these files are located via
```
cd ..;
cd repos/einsteinanalysis/Multipole;
ls;
```
The .ccl files describe the following:

1. configuration.ccl -> what additional thorns are required
1. interface.ccl -> describes the grid functions and functions that can be passed to other thorns 
1. param.ccl -> lists the parameters that can be changed using the parameter file
1. schedule.ccl -> determines when the functions define in the src code are called

Now open `param.ccl`.


### qc0-mclachlan.par
```
Cactus::cctk_run_title = "QC-0"

Cactus::cctk_full_warnings         = yes
Cactus::highlight_warning_messages = no

Cactus::terminate       = "time"
Cactus::cctk_final_time = 200.0



ActiveThorns = "IOUtil"

IO::out_dir = $parfile



ActiveThorns = "AEILocalInterp"

#ActiveThorns = "BLAS LAPACK"

ActiveThorns = "Fortran"

ActiveThorns = "GSL"

ActiveThorns = "GenericFD"

ActiveThorns = "HDF5"

ActiveThorns = "LocalInterp"

ActiveThorns = "LoopControl"

ActiveThorns = "Slab"

ActiveThorns = "TGRtensor"



ActiveThorns = "SummationByParts"

SummationByParts::order = 4



ActiveThorns = "InitBase"



ActiveThorns = "Carpet CarpetLib CarpetInterp CarpetReduce CarpetSlab"

Carpet::verbose           = no
Carpet::veryverbose       = no
Carpet::schedule_barriers = no
Carpet::storage_verbose   = no
#Carpet::timers_verbose    = no
CarpetLib::output_bboxes  = no

Carpet::domain_from_coordbase = yes
Carpet::max_refinement_levels = 10

driver::ghost_size       = 3
Carpet::use_buffer_zones = yes

Carpet::prolongation_order_space = 5
Carpet::prolongation_order_time  = 2

Carpet::convergence_level = 0

Carpet::init_fill_timelevels = yes
#Carpet::init_3_timelevels = yes

Carpet::poison_new_timelevels = yes
CarpetLib::poison_new_memory  = yes

Carpet::output_timers_every      = 5120
CarpetLib::print_timestats_every = 5120
CarpetLib::print_memstats_every  = 5120



ActiveThorns = "NaNChecker"

NaNChecker::check_every     = 1 # 512
#NaNChecker::verbose         = "all"
#NaNChecker::action_if_found = "just warn"
NaNChecker::action_if_found = "terminate"
NaNChecker::check_vars      = "
        ML_BSSN::ML_log_confac
        ML_BSSN::ML_metric
        ML_BSSN::ML_trace_curv
        ML_BSSN::ML_curv
        ML_BSSN::ML_Gamma
        ML_BSSN::ML_lapse
        ML_BSSN::ML_shift
        ML_BSSN::ML_dtlapse
        ML_BSSN::ML_dtshift
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        ADMBase::dtlapse
        ADMBase::dtshift
"



ActiveThorns = "Boundary CartGrid3D CoordBase ReflectionSymmetry RotatingSymmetry180 SymBase"

CoordBase::domainsize = "minmax"

CoordBase::xmin =    0.00
CoordBase::ymin = -120.00
CoordBase::zmin =    0.00
CoordBase::xmax = +120.00
CoordBase::ymax = +120.00
CoordBase::zmax = +120.00
CoordBase::dx   =    2.00
CoordBase::dy   =    2.00
CoordBase::dz   =    2.00

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

CoordBase::boundary_shiftout_x_lower = 1
CoordBase::boundary_shiftout_z_lower = 1

CartGrid3D::type = "coordbase"

ReflectionSymmetry::reflection_z   = yes
ReflectionSymmetry::avoid_origin_z = no



ActiveThorns = "SphericalSurface"

SphericalSurface::nsurfaces = 6
SphericalSurface::maxntheta = 39
SphericalSurface::maxnphi   = 76

SphericalSurface::ntheta      [0] = 39
SphericalSurface::nphi        [0] = 76
SphericalSurface::nghoststheta[0] = 2
SphericalSurface::nghostsphi  [0] = 2

SphericalSurface::ntheta      [1] = 39
SphericalSurface::nphi        [1] = 76
SphericalSurface::nghoststheta[1] = 2
SphericalSurface::nghostsphi  [1] = 2

SphericalSurface::ntheta      [2] = 39
SphericalSurface::nphi        [2] = 76
SphericalSurface::nghoststheta[2] = 2
SphericalSurface::nghostsphi  [2] = 2

SphericalSurface::ntheta      [3] = 39
SphericalSurface::nphi        [3] = 76
SphericalSurface::nghoststheta[3] = 2
SphericalSurface::nghostsphi  [3] = 2

SphericalSurface::ntheta      [4] = 39
SphericalSurface::nphi        [4] = 76
SphericalSurface::nghoststheta[4] = 2
SphericalSurface::nghostsphi  [4] = 2

SphericalSurface::ntheta      [5] = 39
SphericalSurface::nphi        [5] = 76
SphericalSurface::nghoststheta[5] = 2
SphericalSurface::nghostsphi  [5] = 2



ActiveThorns = "CarpetMask"

CarpetMask::verbose = no

CarpetMask::excluded_surface       [0] = 0
CarpetMask::excluded_surface_factor[0] = 1.0

CarpetMask::excluded_surface       [1] = 1
CarpetMask::excluded_surface_factor[1] = 1.0

CarpetMask::excluded_surface       [2] = 2
CarpetMask::excluded_surface_factor[2] = 1.0



ActiveThorns = "CarpetRegrid2 CarpetTracker"

CarpetTracker::surface[0] = 0
CarpetTracker::surface[1] = 1
CarpetTracker::surface[2] = 2

CarpetRegrid2::regrid_every            = 128
CarpetRegrid2::freeze_unaligned_levels = yes
CarpetRegrid2::symmetry_rotating180    = yes
CarpetRegrid2::verbose                 = yes

CarpetRegrid2::num_centres = 3

CarpetRegrid2::num_levels_1         =  7
CarpetRegrid2::position_x_1         = +1.168642873
CarpetRegrid2::radius_1[ 1]         =  64.0
CarpetRegrid2::radius_1[ 2]         =  16.0
CarpetRegrid2::radius_1[ 3]         =   8.0
CarpetRegrid2::radius_1[ 4]         =   4.0
CarpetRegrid2::radius_1[ 5]         =   2.0
CarpetRegrid2::radius_1[ 6]         =   1.0
CarpetRegrid2::movement_threshold_1 =   0.16

CarpetRegrid2::num_levels_2         =  7
CarpetRegrid2::position_x_2         = -1.168642873
CarpetRegrid2::radius_2[ 1]         =  64.0
CarpetRegrid2::radius_2[ 2]         =  16.0
CarpetRegrid2::radius_2[ 3]         =   8.0
CarpetRegrid2::radius_2[ 4]         =   4.0
CarpetRegrid2::radius_2[ 5]         =   2.0
CarpetRegrid2::radius_2[ 6]         =   1.0
CarpetRegrid2::movement_threshold_2 =   0.16

CarpetRegrid2::num_levels_3         =  7
CarpetRegrid2::active_3             = no
CarpetRegrid2::radius_3[ 1]         =  64.0
CarpetRegrid2::radius_3[ 2]         =  16.0
CarpetRegrid2::radius_3[ 3]         =   8.0
CarpetRegrid2::radius_3[ 4]         =   4.0
CarpetRegrid2::radius_3[ 5]         =   2.0
CarpetRegrid2::radius_3[ 6]         =   1.0
CarpetRegrid2::movement_threshold_3 =   0.16



ActiveThorns = "MoL Time"

MoL::ODE_Method             = "RK4"
MoL::MoL_Intermediate_Steps = 4
MoL::MoL_Num_Scratch_Levels = 1

Carpet::time_refinement_factors = "[1, 1, 2, 4, 8, 16, 32, 64, 128, 256]"

Time::dtfac = 0.25



ActiveThorns = "ADMBase ADMCoupling ADMMacros CoordGauge SpaceMask StaticConformal TmunuBase"

ADMMacros::spatial_order = 4



ActiveThorns = "TwoPunctures"

ADMBase::metric_type = "physical"

ADMBase::initial_data    = "twopunctures"
ADMBase::initial_lapse   = "twopunctures-averaged"
ADMBase::initial_shift   = "zero"
ADMBase::initial_dtlapse = "zero"
ADMBase::initial_dtshift = "zero"

# needed for AHFinderDirect
ADMBase::metric_timelevels = 3

TwoPunctures::par_b          =  1.168642873
TwoPunctures::par_m_plus     =  0.453
TwoPunctures::par_m_minus    =  0.453
TwoPunctures::par_P_plus [1] = +0.3331917498
TwoPunctures::par_P_minus[1] = -0.3331917498

#TODO# TwoPunctures::grid_setup_method = "evaluation"

TwoPunctures::TP_epsilon = 1.0e-2
TwoPunctures::TP_Tiny    = 1.0e-2

TwoPunctures::verbose = yes



ActiveThorns = "ML_BSSN ML_BSSN_Helper NewRad"

ADMBase::evolution_method         = "ML_BSSN"
ADMBase::lapse_evolution_method   = "ML_BSSN"
ADMBase::shift_evolution_method   = "ML_BSSN"
ADMBase::dtlapse_evolution_method = "ML_BSSN"
ADMBase::dtshift_evolution_method = "ML_BSSN"

ML_BSSN::harmonicN           = 1      # 1+log
ML_BSSN::harmonicF           = 2.0    # 1+log
ML_BSSN::ShiftGammaCoeff     = 0.75
ML_BSSN::BetaDriver          = 1.0
ML_BSSN::advectLapse         = 1
ML_BSSN::advectShift         = 1

ML_BSSN::MinimumLapse        = 1.0e-8

ML_BSSN::initial_boundary_condition = "extrapolate-gammas"
ML_BSSN::rhs_boundary_condition     = "NewRad"
Boundary::radpower                     = 2

ML_BSSN::ML_log_confac_bound = "none"
ML_BSSN::ML_metric_bound     = "none"
ML_BSSN::ML_Gamma_bound      = "none"
ML_BSSN::ML_trace_curv_bound = "none"
ML_BSSN::ML_curv_bound       = "none"
ML_BSSN::ML_lapse_bound      = "none"
ML_BSSN::ML_dtlapse_bound    = "none"
ML_BSSN::ML_shift_bound      = "none"
ML_BSSN::ML_dtshift_bound    = "none"



ActiveThorns = "Dissipation"

Dissipation::order = 5
Dissipation::vars  = "
        ML_BSSN::ML_log_confac
        ML_BSSN::ML_metric
        ML_BSSN::ML_trace_curv
        ML_BSSN::ML_curv
        ML_BSSN::ML_Gamma
        ML_BSSN::ML_lapse
        ML_BSSN::ML_shift
        ML_BSSN::ML_dtlapse
        ML_BSSN::ML_dtshift
"



ActiveThorns = "ML_ADMConstraints"



ActiveThorns = "WeylScal4 Multipole"
WeylScal4::offset                    = 1e-8 
WeylScal4::fd_order                  = "4th" 
WeylScal4::verbose                   = 0 

Multipole::nradii = 8
Multipole::out_every = 128
Multipole::radius[0] = 15
Multipole::radius[1] = 30
Multipole::radius[2] = 40
Multipole::radius[3] = 50
Multipole::radius[4] = 60
Multipole::radius[5] = 70
Multipole::radius[6] = 80
Multipole::radius[7] = 90
Multipole::variables = "WeylScal4::Psi4r{sw=-2 cmplx='WeylScal4::Psi4i' name='Psi4'}"
Multipole::l_max = 4


ActiveThorns = "AHFinderDirect"

AHFinderDirect::find_every = 128

AHFinderDirect::run_at_CCTK_POST_RECOVER_VARIABLES = no

AHFinderDirect::move_origins            = yes
AHFinderDirect::reshape_while_moving    = yes
AHFinderDirect::predict_origin_movement = yes

AHFinderDirect::geometry_interpolator_name = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars = "order=4"
AHFinderDirect::surface_interpolator_name  = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars  = "order=4"

AHFinderDirect::output_h_every = 0

AHFinderDirect::N_horizons = 6

AHFinderDirect::origin_x                             [1] = +1.168642873
AHFinderDirect::initial_guess__coord_sphere__x_center[1] = +1.168642873
AHFinderDirect::initial_guess__coord_sphere__radius  [1] =  0.25
AHFinderDirect::which_surface_to_store_info          [1] = 0
AHFinderDirect::reset_horizon_after_not_finding      [1] = no
AHFinderDirect::dont_find_after_individual_time      [1] = 30.0

AHFinderDirect::origin_x                             [2] = -1.168642873
AHFinderDirect::initial_guess__coord_sphere__x_center[2] = -1.168642873
AHFinderDirect::initial_guess__coord_sphere__radius  [2] =  0.25
AHFinderDirect::which_surface_to_store_info          [2] = 1
AHFinderDirect::reset_horizon_after_not_finding      [2] = no
AHFinderDirect::dont_find_after_individual_time      [2] = 30.0

AHFinderDirect::initial_guess__coord_sphere__radius  [3] = 1.0
AHFinderDirect::which_surface_to_store_info          [3] = 2
AHFinderDirect::reset_horizon_after_not_finding      [3] = no
AHFinderDirect::find_after_individual_time           [3] = 15.0

AHFinderDirect::surface_definition                   [4] = "expansion product"
AHFinderDirect::surface_selection                    [4] = "areal radius"
AHFinderDirect::desired_value                        [4] = 50.0
AHFinderDirect::initial_guess__coord_sphere__radius  [4] = 50.0
AHFinderDirect::which_surface_to_store_info          [4] = 3
AHFinderDirect::reset_horizon_after_not_finding      [4] = no

AHFinderDirect::depends_on                           [5] = 1
AHFinderDirect::desired_value_offset                 [5] = 0.001
AHFinderDirect::which_surface_to_store_info          [5] = 4
AHFinderDirect::reset_horizon_after_not_finding      [5] = no
AHFinderDirect::dont_find_after_individual_time      [5] = 30.0

AHFinderDirect::depends_on                           [6] = 3
AHFinderDirect::desired_value_offset                 [6] = 0.001
AHFinderDirect::which_surface_to_store_info          [6] = 5
AHFinderDirect::reset_horizon_after_not_finding      [6] = no
AHFinderDirect::find_after_individual_time           [6] = 15.0



ActiveThorns = "QuasiLocalMeasures"

QuasiLocalMeasures::verbose              = yes
QuasiLocalMeasures::interpolator         = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options = "order=4"
QuasiLocalMeasures::spatial_order        = 4

QuasiLocalMeasures::num_surfaces     = 6
QuasiLocalMeasures::surface_index[0] = 0
QuasiLocalMeasures::surface_index[1] = 1
QuasiLocalMeasures::surface_index[2] = 2
QuasiLocalMeasures::surface_index[3] = 3
QuasiLocalMeasures::surface_index[4] = 4
QuasiLocalMeasures::surface_index[5] = 5



ActiveThorns = "CarpetIOBasic"

IOBasic::outInfo_every      = 128
IOBasic::outInfo_reductions = "norm2"
IOBasic::outInfo_vars       = "
        Carpet::physical_time_per_hour
        ML_ADMConstraints::H
        SphericalSurface::sf_radius
        QuasiLocalMeasures::qlm_spin[0]
"



ActiveThorns = "CarpetIOScalar"

IOScalar::one_file_per_group = yes

IOScalar::outScalar_every = 128
IOScalar::outScalar_vars  = "
        CarpetReduce::weight
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        ADMBase::dtlapse
        ADMBase::dtshift
        WEYLSCAL4::Psi4r
        WEYLSCAL4::Psi4i
        ML_ADMConstraints::ML_Ham
        ML_ADMConstraints::ML_mom
        SphericalSurface::sf_radius
        QuasiLocalMeasures::qlm_newman_penrose
        QuasiLocalMeasures::qlm_weyl_scalars
        QuasiLocalMeasures::qlm_ricci_scalars
        QuasiLocalMeasures::qlm_twometric
        QuasiLocalMeasures::qlm_killing_vector
        QuasiLocalMeasures::qlm_killed_twometric
        QuasiLocalMeasures::qlm_invariant_coordinates
        QuasiLocalMeasures::qlm_3determinant
"



ActiveThorns = "CarpetIOASCII"

IOASCII::one_file_per_group = yes

IOASCII::output_symmetry_points = no
IOASCII::out3D_ghosts           = no

IOASCII::out0D_every = 128
IOASCII::out0D_vars  = "
        Carpet::timing
        CarpetReduce::weight
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        ADMBase::dtlapse
        ADMBase::dtshift
        WEYLSCAL4::Psi4r
        WEYLSCAL4::Psi4i
        ML_ADMConstraints::ML_Ham
        ML_ADMConstraints::ML_mom
        SphericalSurface::sf_active
        SphericalSurface::sf_valid
        SphericalSurface::sf_info
        SphericalSurface::sf_radius
        SphericalSurface::sf_origin
        SphericalSurface::sf_coordinate_descriptors
        QuasiLocalMeasures::qlm_state
        QuasiLocalMeasures::qlm_grid_int
        QuasiLocalMeasures::qlm_grid_real
        QuasiLocalMeasures::qlm_scalars
        QuasiLocalMeasures::qlm_multipole_moments
"

IOASCII::out1D_every = 128
IOASCII::out1D_vars  = "
        CarpetReduce::weight
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        ADMBase::dtlapse
        ADMBase::dtshift
        WEYLSCAL4::Psi4r
        WEYLSCAL4::Psi4i
        ML_ADMConstraints::ML_Ham
        ML_ADMConstraints::ML_mom
        SphericalSurface::sf_radius
        QuasiLocalMeasures::qlm_shapes
        QuasiLocalMeasures::qlm_coordinates
        QuasiLocalMeasures::qlm_tetrad_l
        QuasiLocalMeasures::qlm_tetrad_n
        QuasiLocalMeasures::qlm_tetrad_m
        QuasiLocalMeasures::qlm_newman_penrose
        QuasiLocalMeasures::qlm_weyl_scalars
        QuasiLocalMeasures::qlm_ricci_scalars
        QuasiLocalMeasures::qlm_twometric
        QuasiLocalMeasures::qlm_killing_vector
        QuasiLocalMeasures::qlm_killed_twometric
        QuasiLocalMeasures::qlm_invariant_coordinates
        QuasiLocalMeasures::qlm_3determinant
"

IOASCII::out2D_every = 128
IOASCII::out2D_vars  = "
        SphericalSurface::sf_radius
"



Activethorns = "CarpetIOHDF5"

IOHDF5::out_every              = 512
IOHDF5::one_file_per_group     = yes
IOHDF5::output_symmetry_points = no
IOHDF5::out3D_ghosts           = no
IOHDF5::compression_level      = 1
IOHDF5::use_checksums          = yes
IOHDF5::out_vars               = "
        CarpetReduce::weight
        ADMBase::metric
        ADMBase::curv
        ADMBase::lapse
        ADMBase::shift
        ADMBase::dtlapse
        ADMBase::dtshift
        WEYLSCAL4::Psi4r
        WEYLSCAL4::Psi4i
        ML_ADMConstraints::ML_Ham
        ML_ADMConstraints::ML_mom
"

IOHDF5::checkpoint                  = yes
IO::checkpoint_dir                  = $parfile
IO::checkpoint_ID                   = yes
IO::checkpoint_every_walltime_hours = 6.0
IO::checkpoint_on_terminate         = yes

IO::recover     = "autoprobe"
IO::recover_dir = $parfile



ActiveThorns = "Formaline"



ActiveThorns = "TimerReport"

TimerReport::out_every                  = 5120
TimerReport::out_filename               = "TimerReport"
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::n_top_timers               = 20
```